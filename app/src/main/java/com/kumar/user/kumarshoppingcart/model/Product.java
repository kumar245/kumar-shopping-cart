package com.kumar.user.kumarshoppingcart.model;

import com.kumar.user.kumarshoppingcart.Data.SampleCustomData;

/**
 * Created by User on 3/18/2017.
 */

public class Product {
    private long id;
    private String productName;
    private String description;
    private String promoMessage;
    private String imagePath;
    private double salesPrice;
    private double purchasePrice;
    private long categoryId;
    private String categoryName;
    private long dateAdded;
    private long dateOfLastTransaction;

   public Product(){

   }
    public Product(Product product){

        this.id =product.getId();
        this.productName=product.getProductName();
        this.description=product.getDescription();
        this.promoMessage=product.getPromoMessage();
        this.imagePath=product.getImagePath();
        this.salesPrice=product.getSalesPrice();
        this.purchasePrice=product.getPurchasePrice();
        this.categoryId=product.getCategoryId();
        this.categoryName=product.getCategoryName();
        this.dateAdded=product.getDateAdded();
        this.dateOfLastTransaction=product.getDateOfLastTransaction();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPromoMessage() {
        return promoMessage;
    }

    public void setPromoMessage(String promoMessage) {
        this.promoMessage = promoMessage;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public double getSalesPrice() {
        return salesPrice;
    }

    public void setSalesPrice(double salesPrice) {
        this.salesPrice = salesPrice;
    }

    public double getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(double purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public long getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(long dateAdded) {
        this.dateAdded = dateAdded;
    }

    public long getDateOfLastTransaction() {
        return dateOfLastTransaction;
    }

    public void setDateOfLastTransaction(long dateOfLastTransaction) {
        this.dateOfLastTransaction = dateOfLastTransaction;
    }


}
