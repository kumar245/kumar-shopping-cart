package com.kumar.user.kumarshoppingcart.core.events;

import com.kumar.user.kumarshoppingcart.model.LineItem;

import java.util.List;

/**
 * Created by User on 5/10/2017.
 */

public class UpdatedToolBarEvent {
    private final List<LineItem> mLineItems;

    public UpdatedToolBarEvent(List<LineItem> mLineItems) {
        this.mLineItems = mLineItems;
    }

    public List<LineItem> getmLineItems() {
        return mLineItems;
    }
}
