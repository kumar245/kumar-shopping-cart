package com.kumar.user.kumarshoppingcart.core;

import android.app.Application;

import com.kumar.user.kumarshoppingcart.core.Dagger.AppComponent;
import com.kumar.user.kumarshoppingcart.core.Dagger.AppModule;
import com.kumar.user.kumarshoppingcart.core.Dagger.DaggerAppComponent;

/**
 * Created by User on 5/2/2017.
 */

public class ProntoShopApplication extends Application {
    private static AppComponent appComponent;
    private static ProntoShopApplication instance= new ProntoShopApplication();

    public static ProntoShopApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getAppComponent();


}

   public AppComponent getAppComponent(){
       if (appComponent==null)
       {
          appComponent =  DaggerAppComponent.builder()
                   .appModule(new AppModule(this))
                   .build();


       }
       return appComponent;
    }

}
