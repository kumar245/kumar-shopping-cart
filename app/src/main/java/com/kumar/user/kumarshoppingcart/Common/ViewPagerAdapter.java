package com.kumar.user.kumarshoppingcart.Common;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.kumar.user.kumarshoppingcart.ui.Customerlist.CustomerListFragment;
import com.kumar.user.kumarshoppingcart.ui.checkout.checkoutFragment;
import com.kumar.user.kumarshoppingcart.ui.productlist.ProductListFragment;

/**
 * Created by User on 4/25/2017.
 */

public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        Fragment selectedFragment;
        switch (position){
            case 0:
                selectedFragment= new ProductListFragment();
                break;
            case 1:
                selectedFragment=new CustomerListFragment();
                break;
            case 2:
                selectedFragment=new checkoutFragment();
                break;
            default:
                selectedFragment=new ProductListFragment();
                break;

        }
        return selectedFragment;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title ="";
        switch (position){
            case 0:
                title= "Products";
                break;
            case 1:
                title="Customers";
                break;
            case 2:
                title="Shopping Cart";
                break;
        }
        return title;
    }
}
