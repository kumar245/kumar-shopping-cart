package com.kumar.user.kumarshoppingcart.core.listeners;

import com.kumar.user.kumarshoppingcart.model.LineItem;

/**
 * Created by User on 4/23/2017.
 */

public interface CartActionsListener {

    void onItemDeleted(LineItem item);
    void onItemQtyChange(LineItem item, int qty);

}
