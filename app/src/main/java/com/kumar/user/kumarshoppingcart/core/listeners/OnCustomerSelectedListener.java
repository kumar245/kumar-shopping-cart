package com.kumar.user.kumarshoppingcart.core.listeners;

import com.kumar.user.kumarshoppingcart.model.Customer;

/**
 * Created by User on 4/19/2017.
 */

public interface OnCustomerSelectedListener {
    void onSelectCustomer(Customer customer);
    void onLongClickCustomer(Customer customer);
}
