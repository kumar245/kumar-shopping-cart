package com.kumar.user.kumarshoppingcart.core.Dagger;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.kumar.user.kumarshoppingcart.Common.ShoppingCart;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by User on 5/2/2017.
 */
@Module
public class ShoppingCartModule {

    @Provides @Singleton
    SharedPreferences providesSharedPreferences(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context);
    }
    @Provides @Singleton
    ShoppingCart providesShoppingCart(SharedPreferences preferences){
        return new ShoppingCart(preferences);
    }
}
