package com.kumar.user.kumarshoppingcart.ui.addProduct;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kumar.user.kumarshoppingcart.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class add_Product_Dialog_Fragment extends Fragment {


    public add_Product_Dialog_Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add__product__dialog_, container, false);
    }

}
