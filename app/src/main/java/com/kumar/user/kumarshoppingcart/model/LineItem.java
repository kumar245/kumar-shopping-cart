package com.kumar.user.kumarshoppingcart.model;

/**
 * Created by User on 3/19/2017.
 */

public class
LineItem extends Product {
    private int quantity;

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    private double getSumPrice(){
      return   getSalesPrice()*quantity;
    }
}
