package com.kumar.user.kumarshoppingcart.ui.checkout;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.kumar.user.kumarshoppingcart.R;
import com.kumar.user.kumarshoppingcart.core.listeners.CartActionsListener;
import com.kumar.user.kumarshoppingcart.model.LineItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class checkoutFragment extends Fragment implements CartActionsListener {
    private View mRootview;
    private CheckoutAdapter mAdapter;
    @BindView(R.id.checkout_recyclerview) RecyclerView mRecyclerView;
    @BindView(R.id.empty_text) TextView mEmptyTextView;
    @BindView(R.id.clear_cart_button) Button mClearButton;
    @BindView(R.id.checkout_cart_button) Button mCheckoutButton;
    @BindView(R.id.textView_of_subTotal) TextView mSubTotalTextView;
    @BindView(R.id.textView_total) TextView mTotalTextView;
    @BindView(R.id.textView_tax) TextView mTotalTaxRate;
    @BindView(R.id.radio_group_payment_type) RadioGroup mRadioGroup;


    public checkoutFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mRootview= inflater.inflate(R.layout.fragment_checkout, container, false);
        ButterKnife.bind(this,mRootview);
        List<LineItem> tempItems = new ArrayList<>();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        mAdapter=new CheckoutAdapter(tempItems,getActivity(),this);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mAdapter);
        if (tempItems.size()<1){
            showEmptyTextMessage();
        }
        else {
            hideEmptyTextMessage();
        }
        return mRootview;

    }
    private void hideEmptyTextMessage() {
        mRecyclerView.setVisibility(View.GONE);
        mEmptyTextView.setVisibility(View.VISIBLE);
    }

    private void showEmptyTextMessage() {
        mRecyclerView.setVisibility(View.GONE);
        mEmptyTextView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onItemDeleted(LineItem item) {

    }

    @Override
    public void onItemQtyChange(LineItem item, int qty) {

    }
}
