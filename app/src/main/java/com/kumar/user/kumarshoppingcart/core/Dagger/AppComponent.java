package com.kumar.user.kumarshoppingcart.core.Dagger;

import com.kumar.user.kumarshoppingcart.Common.MainActivity;
import com.kumar.user.kumarshoppingcart.Common.ShoppingCart;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by User on 3/18/2017.
 */
@Singleton
@Component(
        modules = {AppModule.class,
                    ShoppingCartModule.class,
                    BusModule.class}
)
public interface AppComponent {
    void inject(MainActivity activity);
    void inject(ShoppingCart cart);
}
