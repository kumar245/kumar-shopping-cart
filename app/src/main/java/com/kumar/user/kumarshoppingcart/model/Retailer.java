package com.kumar.user.kumarshoppingcart.model;

/**
 * Created by User on 3/19/2017.
 */

public class Retailer {
    private int id;
    private String businessName;
    private String emailAddress;
    private String phoneNumber;
    private String streetAddress;
    private String streetAddress2;
    private String city;
    private String state;
    private String zip;
    private String industry;
    private String contactPerson;

    public Retailer() {

    }
}
