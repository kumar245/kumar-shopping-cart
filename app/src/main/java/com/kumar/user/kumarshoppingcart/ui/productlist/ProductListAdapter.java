package com.kumar.user.kumarshoppingcart.ui.productlist;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kumar.user.kumarshoppingcart.R;
import com.kumar.user.kumarshoppingcart.Utils.Formatter;
import com.kumar.user.kumarshoppingcart.core.listeners.OnProductSelectedListener;
import com.kumar.user.kumarshoppingcart.model.Product;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by User on 4/17/2017.
 */

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ViewHolder>   {

    private  List<Product> mProducts;
    private final Context mContext;
    private final OnProductSelectedListener mListener;


    public ProductListAdapter(List<Product> products, Context context, OnProductSelectedListener mListener) {
        this.mProducts = products;
        this.mContext = context;
        this.mListener = mListener;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

       View rowView= LayoutInflater.from(parent.getContext()).inflate(R.layout.row_product_list,parent,false);
        ViewHolder viewHolder = new ViewHolder(rowView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (mProducts!=null) {
            Product product = mProducts.get(position);
            Picasso.with(mContext)
                    .load(product.getImagePath())
                    .fit()
                    .placeholder(R.drawable.default_image)
                    .into(holder.productImage);
            holder.productName.setText(product.getPromoMessage());
            holder.category.setText(product.getCategoryName());
            holder.productPrice.setText(Formatter.formatCurrency(product.getSalesPrice()));
            String productDescription=product.getDescription();
            String shortDescription=productDescription.substring(0,Math.min(productDescription.length(),70));
            holder.description.setText(shortDescription);
        }

    }

    @Override
    public int getItemCount() {
        if (mProducts!=null) {
            return mProducts.size();
        } else {
            return 0;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener,View.OnLongClickListener{


        @BindView(R.id.product_image) ImageView productImage;
         @BindView(R.id.textview_product_category) TextView category;
        @BindView(R.id.textview_product_name) TextView productName;
        @BindView(R.id.textview_product_description)TextView description;
        @BindView(R.id.image_view_add_to_cart_button) ImageView addToCartButton;
        @BindView(R.id.text_view_product_price) TextView productPrice;

       public ViewHolder(View itemView) {
            super(itemView);
           ButterKnife.bind(this,itemView);
           addToCartButton.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
        Product selectedProduct=mProducts.get(getLayoutPosition());
            mListener.OnSelectedProduct(selectedProduct);
        }

        @Override
        public boolean onLongClick(View v) {
            Product clickedProduct=mProducts.get(getLayoutPosition());
            mListener.OnLongClickProduct(clickedProduct);
            return true;
        }
    }

}
