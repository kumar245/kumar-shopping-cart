package com.kumar.user.kumarshoppingcart.Common;

import com.kumar.user.kumarshoppingcart.model.Customer;
import com.kumar.user.kumarshoppingcart.model.LineItem;

import java.util.List;

/**
 * Created by User on 4/26/2017.
 */

public interface ShoppingCartContract {
    void addItemToCart(LineItem item);
    void removedItemFromCart(LineItem item);
    void clearAllItemsFromCart();
    List<LineItem>  getShoppingCart();
    void setCustomer(Customer customer);
    void updateItemQty(LineItem item, int qty);
    Customer getSelectedCustomer();
    void completeCheckout();



}
