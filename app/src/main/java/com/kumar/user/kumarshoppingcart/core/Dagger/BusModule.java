package com.kumar.user.kumarshoppingcart.core.Dagger;

import com.squareup.otto.Bus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by User on 5/3/2017.
 */
@Module
public class BusModule {

    @Provides @Singleton
    public Bus provideBus(){
        return new Bus();
    }
}
