package com.kumar.user.kumarshoppingcart.core.Dagger;

import android.content.Context;

import com.kumar.user.kumarshoppingcart.core.ProntoShopApplication;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by User on 3/18/2017.
 */
@Module
public class AppModule {
    private final ProntoShopApplication app;

    public AppModule(ProntoShopApplication app) {
        this.app = app;
    }
    @Provides @Singleton
    public ProntoShopApplication provideApp(){
        return app;
    }
    @Provides @Singleton
    public Context provideContext(){
    return app;
    }


}
