package com.kumar.user.kumarshoppingcart.core.events;

import com.kumar.user.kumarshoppingcart.model.Customer;

/**
 * Created by User on 5/10/2017.
 */

public class CustomerSelectedEvent {
    private final Customer selectedCustomer;
    private final boolean clearCustomer;


    public CustomerSelectedEvent(Customer selectedCustomer, boolean clearCustomer) {
        this.selectedCustomer = selectedCustomer;
        this.clearCustomer = clearCustomer;
    }

    public Customer getSelectedCustomer() {
        return selectedCustomer;
    }

    public boolean isClearCustomer() {
        return clearCustomer;
    }
}
