package com.kumar.user.kumarshoppingcart.Utils;

/**
 * Created by User on 3/18/2017.
 */

public class Constants {
    public static final String SERIALIZED_CART_ITEMS ="serialized_cart_items";
    public static final String SERIALIZED_CUSTOMER_ITEMS = "serialized_customer_items" ;
    public static final String OPEN_CART_EXISTS = "open_cart_exists";
}
