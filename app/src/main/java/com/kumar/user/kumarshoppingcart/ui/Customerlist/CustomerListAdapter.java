package com.kumar.user.kumarshoppingcart.ui.Customerlist;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kumar.user.kumarshoppingcart.R;
import com.kumar.user.kumarshoppingcart.model.Customer;
import com.squareup.picasso.Picasso;
import com.kumar.user.kumarshoppingcart.core.listeners.OnCustomerSelectedListener;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by User on 4/18/2017.
 */

public class CustomerListAdapter extends RecyclerView.Adapter<CustomerListAdapter.ViewHolder>  {
    private final List<Customer> mCustomers;
    private final Context mContext;
    private  final OnCustomerSelectedListener mListener;
    private boolean shouldHighLightSelectedRow = false;
    private int selectedPosition=0;

    public CustomerListAdapter(List<Customer> customers, Context context, OnCustomerSelectedListener mListener) {
        this.mCustomers = customers;
        this.mContext =  context;
        this.mListener = mListener;
    }

    @Override
    public CustomerListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rowView= LayoutInflater.from(parent.getContext()).inflate(R.layout.row_customer_list,parent,false);
        ViewHolder viewHolder=new ViewHolder(rowView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CustomerListAdapter.ViewHolder holder, int position) {

        final Customer selectedCustomer=mCustomers.get(position);
        holder.customerName.setText(selectedCustomer.getCustomerName());
        holder.customerEmail.setText(selectedCustomer.getEmailAddress());
        Picasso.with(mContext)
        .load(selectedCustomer.getProfileImagePath())
        .fit()
        .placeholder(R.drawable.profile_icon)
        .into(holder.customerHeadShot);
        if (shouldHighLightSelectedRow){
            if (selectedPosition==position){
                holder.itemView.setBackgroundColor(ContextCompat.getColor(mContext,R.color.primary_light));

            }
            else {
                holder.itemView.setBackgroundColor(Color.TRANSPARENT);
            }

        }
        else {
            holder.itemView.setBackgroundColor(Color.TRANSPARENT);

        }

    }

    @Override
    public int getItemCount() {
        if (mCustomers!=null) {
            return mCustomers.size();
        } else {
            return 0;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener,View.OnLongClickListener{
        @BindView(R.id.image_view_customer_head_short) ImageView customerHeadShot;
        @BindView(R.id.textView_customer_email) TextView customerEmail;
        @BindView(R.id.textView_customer_name) TextView customerName;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        @Override
        public void onClick(View v) {
        shouldHighLightSelectedRow = true;
            selectedPosition = getLayoutPosition();
            Customer selectedCustomer = mCustomers.get(selectedPosition);
            mListener.onSelectCustomer(selectedCustomer);
            notifyDataSetChanged();


        }

        @Override
        public boolean onLongClick(View v) {
            Customer selectedCustomer= mCustomers.get(selectedPosition);
            mListener.onLongClickCustomer(selectedCustomer);
            return true;
        }
    }
}
