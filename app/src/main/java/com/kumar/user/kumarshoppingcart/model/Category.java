package com.kumar.user.kumarshoppingcart.model;

/**
 * Created by User on 3/19/2017.
 */

public class Category {
    private int id;
    private String categoryName;

    public Category() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
}
