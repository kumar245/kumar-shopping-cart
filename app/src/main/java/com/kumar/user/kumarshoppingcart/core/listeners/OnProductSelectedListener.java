package com.kumar.user.kumarshoppingcart.core.listeners;

import com.kumar.user.kumarshoppingcart.model.Product;

/**
 * Created by User on 4/18/2017.
 */

public interface OnProductSelectedListener {
    void OnSelectedProduct(Product selectedProduct);
    void OnLongClickProduct(Product clickedProduct);
}
